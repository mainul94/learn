import os
import zipfile

def zipdir(path, ziph, replace=''):
    # ziph is zipfile handle
    if os.path.isfile(path):
    	ziph.write(os.path.join(path).replace(replace,'',1))
    else:
	    for root, dirs, files in os.walk(path):
	        for file in files:
	            ziph.write(os.path.join(root, file).replace(replace,'',1))

if __name__ == '__main__':
    zipf = zipfile.ZipFile('Python.zip', 'w')
    zipdir('/media/mainul/Ubuntu/py/learn/env/', zipf, '/media/mainul/Ubuntu/py/learn/')
    zipf.close()