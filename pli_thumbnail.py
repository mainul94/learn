from PIL import Image
from PIL import ImageDraw
im = Image.open("5842-people-vintage-photo-memories.jpg")
im.thumbnail((300,300))
box = im.getbbox()
im2=im.crop((box[0], box[1], box[2], box[3]+50))
draw=ImageDraw.Draw(im2)
draw.text((box[2]-100,box[3]+10), "Copyright")
im2.show()
from psd_tools import PSDImage
psd = PSDImage.load('back.psd')
marge_image = psd.as_PIL()
marge_image.thumbnail((300,300))
marge_image.show()